import React, { useState } from "react";
import { AutoComplete } from "primereact/autocomplete";
export const Welcome = (prop: any) => {
  const [value, setValue] = useState("");

  return (
    <h1>
      Welcome {prop.name}{" "}
      <AutoComplete value={value} onChange={(e) => setValue(e.value)} />
    </h1>
  );
};
