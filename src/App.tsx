import React from "react";
import "./App.css";
import Display from "./Components/Basic";
import { Welcome } from "./Components/Welcome";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { BrowserRouter, Routes, Route, Outlet, Link } from "react-router-dom";
function App() {
  return (
    <BrowserRouter>
      <header>
        <div className="d-flex">
          <Link to="/">Home</Link> <Link to="/welcome">Welcome</Link>
          {"  "}
          <Link to="/blogs">Table</Link>
        </div>
        <Outlet />
      </header>

      <Routes>
        <Route index element={<h1>Hello</h1>} />
        <Route path="welcome" element={<Welcome name="User" />} />
        <Route path="blogs" element={<Display />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
